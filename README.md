# arontier/ad3/documents/galaxydock3-examples

This is a repository for galaxydock3 app example files.

## Prerequisites

You should have 

* `<api-key-string>`: API key of your AD3 REST API client machine
* `<token-string>`: a user's auth token

to consume AD3 REST API endpoints. And
you should do the following demonstration on your AD3 REST API client machine.

## How to use

### Getting examples

First git-clone this repository and change directory into it:

```bash
git clone https://gitlab.com/arontier/coworkers/cimplrx/galaxydock3-examples
cd galaxydock3-examples/examples
```

### Submitting work
To submit a work, run:

```bash
curl --location --request POST 'https://rest.ad3.io/api/v1/galaxydock3/works/' \
  --header 'X-API-KEY: <api-key-string>' \
  --header 'Authorization: Bearer <token-string>' \
  --form 'description=testing-galaxydock3' \
  --form 'receptor=@input/2gl0_receptor.pdb' \
  --form 'ligand=@input/2gl0_corina.mol2' \
  --form 'x_center=14.875' \
  --form 'y_center=20.035' \
  --form 'z_center=11.656'
```

If our AD3 REST API server is not heavily loaded, it will finish this example in about 8 minutes.

### Listing works

To list all submitted works, run:

```bash
curl --location --request GET 'https://rest.ad3.io/api/v1/galaxydock3/works/' \
  --header 'X-API-KEY: <api-key-string>' \
  --header 'Authorization: Bearer <token-string>'
```

### Getting work back

To see specific work, run:

```bash
curl --location --request GET 'https://rest.ad3.io/api/v1/galaxydock3/works/<work-id>/' \
  --header 'X-API-KEY: <api-key-string>' \
  --header 'Authorization: Bearer <token-string>'
```

where

* `<work-id>` is the value of `id` returned in the response of "listing works" API request.

A download link `outputs_link` for your submitted work is in the response of the above request.
Download the result using this link and compare with the one in `output` directory.

## References

* REST API Documentation: [https://docs.ad3.io/#/galaxydock3](https://docs.ad3.io/#/galaxydock3)

You may need a read access privilege to see the following references:

* Source repository: [https://github.com/arontier/libarontier](https://github.com/arontier/libarontier)
* Docker, workflow repository: [https://gitlab.com/arontier/ad3/galaxydock3](https://gitlab.com/arontier/ad3/galaxydock3)
